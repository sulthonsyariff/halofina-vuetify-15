import BannerPopup from '../components/others/BannerPopup'

export default [
  {
    path: '/others/banner',
    components: BannerPopup
  }
]
