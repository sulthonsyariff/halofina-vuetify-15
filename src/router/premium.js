const Premium = () => import('@/components/v2/premium/Premium')
const NotPremium = () => import('@/components/v2/premium/NotPremium')

export default [
  {
    path: '/premium',
    component: Premium
  },
  {
    path: '/notpremium',
    component: NotPremium
  }
]
