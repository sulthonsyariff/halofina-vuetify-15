import Vue from 'vue'
import Router from 'vue-router'
// import Api from '@/services/Api'

// module router
import finacheck from './finacheck'
import finapedia from './finapedia'
import finavest from './finavest'
import premium from './premium'
import others from './others'

// import feature lists
import { checkFeatures } from '@/services/Helpers'

// Component
import Splashscreen from '@/components/Splashscreen'
// import Welcome from '@/components/v2/onboarding2/Welcome'
// import OnboardingAlreadyInvest from '@/components/v2/onboarding/AlreadyInvest'
// import OnboardingNotInvest from '@/components/v2/onboarding/NotInvest'
// import OnboardingCalculator from '@/components/v2/onboarding/Calculator'
// import OnboardingInflasi from '@/components/v2/onboarding/Inflasi'
import Chatting from '@/components/v2/premium/Chatting'
import Qiscus from '@/components/Qiscus'
import Shimering from '@/components/v2/shimmering/Profile'
import Something from '@/components/others/Something'
const Dashboard = () => import('@/components/Dashboard')
const Help = () => import('../components/Help')
const Partners = () => import('../components/Partners')
const ReferalCode = () => import('@/components/v2/profile/ReferalCode')
const Snk = () => import('@/components/v2/profile/Snk')
const SettingsProfile = () => import('@/components/v2/profile/Settings')
const Beranda = () => import('@/components/v2/premium/Beranda')
const AddVoucher = () => import('@/components/v2/profile/voucher/AddVoucher')
const CompletedVoucher = () => import('@/components/v2/profile/voucher/Completed')

Vue.use(Router)

const router = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: Splashscreen
    },
    // {
    //   path: '/onboarding',
    //   component: Onboarding
    // },
    // {
    //   path: '/onboarding2',
    //   component: Onboarding2
    // },
    // {
    //   path: '/onboarding2/welcome',
    //   component: Welcome
    // },
    // {
    //   path: '/onboardingv1',
    //   component: OnboardingV1
    // },
    // {
    //   path: '/onboarding/already',
    //   component: OnboardingAlreadyInvest
    // },
    // {
    //   path: '/onboarding/not',
    //   component: OnboardingNotInvest
    // },
    // {
    //   path: '/onboarding/calculator',
    //   component: OnboardingCalculator
    // },
    // {
    //   path: '/onboarding/inflasi',
    //   component: OnboardingInflasi
    // },
    {
      path: '/help',
      component: Help
    },
    {
      path: '/partners',
      component: Partners
    },
    {
      path: '/qiscus',
      component: Qiscus
    },
    {
      path: '/finaconsult/beranda',
      component: Beranda
    },
    {
      path: '/finaconsult/chatting/:email',
      props: true,
      component: Chatting
    },
    {
      path: '/shimering',
      component: Shimering
    },
    {
      path: '/profile/voucher/add',
      component: AddVoucher
    },
    {
      path: '/profile/voucher/completed',
      component: CompletedVoucher
    },
    {
      path: '/dashboard',
      component: Dashboard,
      props: (route) => ({
        tab: route.query.tab
      })
    },
    {
      path: '/profile/referalcode/dashboard',
      component: ReferalCode
    },
    {
      path: '/profile/referalcode/snk',
      component: Snk
    },
    {
      path: '/profile/settings',
      component: SettingsProfile
    },
    {
      path: '/loaderio-ca54a48ace8ba04186a0110c623d5ac0',
      component: Something
    }
  ].concat(
    finacheck,
    finavest,
    finapedia,
    premium,
    others
  ),
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})

// global routing check

// async function checkToken () {
//   Api('finacheck/rpa/user', {})
//     .then(() => {
//       return true
//     })
//     .catch((err) => {
//       if (err.m === 'Unauthorized Access' || err.m === 'User tidak dikenal' || err.m === 'User tidak aktif' || err.m === 'The JWT string must have two dots') {
//         localStorage.removeItem('halofina-token')
//         return false
//       }
//     })
// }

router.beforeEach((to, from, next) => {
  // let isAuthorized = checkToken()
  const canSetRpa = checkFeatures('halofina:risk_profile').enabled
  const canSetSpending = checkFeatures('halofina:saving_profile').enabled
  const canSetLifeplan = checkFeatures('halofina:lifeplan_creator').enabled
  const canSetKyc = checkFeatures('halofina:kyc_page').enabled
  const canSeeHistory = checkFeatures('halofina:transaction_history').enabled
  const canSetPassword = checkFeatures('halofina:password').enabled
  const canSetOtp = checkFeatures('halofina:email_otp').enabled
  // const canAccessNewTrx = checkFeatures('halofina:trx').version
  const kycVersion = checkFeatures('halofina:kyc_page').version
  const hasRpa = localStorage.getItem('halofina-rpa')
  const hasSaving = localStorage.getItem('halofina-saving-profile')
  // console.log(isAuthorized)
  // Auth
  // if (isAuthorized === false && to.path !== '/loaderio-ca54a48ace8ba04186a0110c623d5ac0') {
  //   console.log('nope')
  //   next('/onboarding')
  // } else {
  //   console.log('nopeee')
  //   next()
  // }
  // KYC version
  if (kycVersion > 1 && to.path === '/finacheck/kyc/list') {
    next('/v2/input/finacheck/kyc/list/new')
  } else {
    next()
  }
  if (kycVersion > 1 && to.path === '/finacheck/kyc/data_kyc') {
    next('/profile/settings')
  } else {
    next()
  }
  // can access new trx
  // if (canAccessNewTrx < 3 && to.path === '/v2/finavest/investment/buy/buy_investment/' + to.params.id + '/new') {
  //   next('/finavest/investment/buy/buy_investment/' + to.params.id + '/new')
  // } else {
  //   next()
  // }
  // can access rpa
  if (canSetRpa === false && to.path === '/finacheck/rpa/input') {
    next('/dashboard')
  } else {
    next()
  }
  // has rpa
  if ((hasRpa === 'null' || hasSaving === 'null') && (to.path === '/v2/finacheck/kyc/list' || to.path === '/finacheck/kyc/list' || to.path === '/finavest/new/lifeplan/add')) {
    next('/new/finacheck/rpa/input/not')
  } else {
    next()
  }
  // can access spending
  if (canSetSpending === false && to.path === '/finacheck/kyc/spending') {
    next('/dashboard')
  } else {
    next()
  }
  // can access lifeplan
  if (canSetLifeplan === false && (to.path === '/finavest/lifeplan/template' || to.path === '/finavest/lifeplan/template/edit' || to.path === '/finavest/lifeplan/add')) {
    next('/dashboard')
  } else {
    next()
  }
  // can access kyc
  if (canSetKyc === false && to.path === '/finacheck/kyc/list') {
    next('/dashboard')
  } else {
    next()
  }
  // can access history transaction
  if (canSeeHistory === false && to.path === '/finacheck/kyc/spending') {
    next('/dashboard')
  } else {
    next()
  }
  // can access password
  if (canSetPassword === false && (to.path === '/password/forget' || to.path === '/password/new' || to.path === '/password/change' || to.path === '/login/password')) {
    if (localStorage.getItem('halofina-is-enabled') === 'true') {
      next('/otp/type/login')
    } else {
      next('/activation/signup')
    }
  } else {
    next()
  }
  // can access spending
  if (canSetOtp === false && to.path === '/otp/type/register') {
    next('/dashboard')
  } else {
    next()
  }
})

export default router
