// import DashboardFinapedia from '../components/finapedia/Dashboard'
// const Glossary = () => import('../components/finapedia/Glossary')
// const Section = () => import('../components/finapedia/course/Section')
// const Lesson = () => import('../components/finapedia/course/Lesson')
// const Article = () => import('../components/finapedia/Article')
// const ArticleDetail = () => import('../components/finapedia/article/Detail')
// V2
import DashboardFinapedia from '../components/v2/finapedia/Dashboard'
const Glossary = () => import('../components/v2/finapedia/Glossary')
const Section = () => import('../components/v2/finapedia/course/Section')
const Lesson = () => import('../components/v2/finapedia/course/Lesson')
const Article = () => import('../components/v2/finapedia/Article')
const ArticleDetail = () => import('../components/v2/finapedia/article/Detail')
const PodcastList = () => import('../components/v2/finapedia/podcast/PodcastList')
const PodcastDetail = () => import('../components/v2/finapedia/podcast/PodcastDetail')
const CourseList = () => import('../components/v2/finapedia/course/CourseList')

export default [
  {
    path: '/finapedia/dashboard',
    component: DashboardFinapedia
  },
  {
    path: '/finapedia/glossary',
    component: Glossary
  },
  {
    path: '/finapedia/article',
    component: Article
  },
  {
    path: '/finapedia/course/lesson/:lessonid',
    name: 'Lesson',
    component: Lesson
  },
  {
    path: '/finapedia/course/section/:courseid',
    name: 'Section',
    component: Section
  },
  {
    path: '/finapedia/article/:articleid',
    name: 'Article',
    component: ArticleDetail
  },
  {
    path: '/finapedia/podcast/list',
    name: 'Podcast List',
    component: PodcastList
  },
  {
    path: '/finapedia/podcast/detail/:id',
    name: 'Podcast Detail',
    props: true,
    component: PodcastDetail
  },
  {
    path: '/finapedia/course/list',
    name: 'Course List',
    component: CourseList
  }
]
