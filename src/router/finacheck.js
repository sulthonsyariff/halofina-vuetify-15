import SignUp from '@/components/finacheck/SignUp'

import Activation from '@/components/finacheck/Activation'
import V2Router from '@/components/v2/finacheck/router.js'

// V1
const Personal = () => import('@/components/finacheck/Personal')
const NewPersonal = () => import('@/components/v2/finacheck/Personal')
const Otp = () => import('@/components/finacheck/Otp')
const LoginUpdate = () => import('@/components/finacheck/login/Update')
const Password = () => import('@/components/finacheck/Password')
const KycSpending = () => import('@/components/finacheck/kyc/Spending')
const NewKycSpending = () => import('@/components/v2/finacheck/kyc/Spending')
const KycBank = () => import('@/components/finacheck/kyc/Bank')
const KycPersonal = () => import('@/components/finacheck/kyc/Personal')
const KycSK = () => import('@/components/finacheck/kyc/SK')
const KycList = () => import('@/components/finacheck/kyc/ListKyc')
const KycData = () => import('@/components/finacheck/kyc/DataKyc')
const KycAddress = () => import('@/components/finacheck/kyc/Address')
// const RpaInput = () => import('@/components/finacheck/rpa/Input')

export default [
  // KYC
  {
    path: '/otp',
    component: Otp
  },
  {
    path: '/otp/:pin',
    component: Otp
  },
  {
    path: '/otp/type/:type',
    component: Otp
  },
  {
    path: '/signup',
    component: SignUp
  },
  {
    path: '/activation/:type',
    component: Activation
  },
  {
    path: '/password/:type',
    props: true,
    component: Password
  },
  {
    path: '/personal',
    component: Personal
  },
  {
    path: '/v2/personal',
    component: NewPersonal
  },
  // {
  //   path: '/finacheck/rpa/input/:id',
  //   component: RpaInput,
  //   props: true
  // },
  {
    path: '/finacheck/kyc/spending/:id',
    component: KycSpending,
    props: true
  },
  {
    path: '/new/finacheck/kyc/spending/:id',
    component: NewKycSpending,
    props: true
  },
  {
    path: '/finacheck/kyc/spending',
    component: KycSpending
  },
  {
    path: '/finacheck/kyc/bank',
    component: KycBank
  },
  {
    path: '/finacheck/kyc/sk',
    component: KycSK
  },
  {
    path: '/finacheck/kyc/personal',
    component: KycPersonal
  },
  {
    path: '/finacheck/kyc/list',
    component: KycList
  },
  {
    path: '/finacheck/kyc/data_kyc',
    component: KycData
  },
  {
    path: '/finacheck/kyc/address',
    component: KycAddress
  },

  // RPA
  // {
  //   path: '/finacheck/rpa/input',
  //   component: RpaInput
  // },

  // V2
  {
    path: '/login/update',
    component: LoginUpdate
  }
].concat(
  V2Router.password,
  V2Router.acquisitions,
  V2Router.pin,
  V2Router.rpa,
  V2Router.kyc,
  V2Router.fqt,
  V2Router.fcu
)
