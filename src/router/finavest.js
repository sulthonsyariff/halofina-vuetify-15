// module export
// V1
import V1Router from '@/components/finavest/router.js'
// V2
import V2Router from '@/components/v2/finavest/router.js'
// Others
const BannerPopup = () => import('../components/others/BannerPopup')
const BannerDetail = () => import('../components/others/BannerDetail')
export default [
  // Others
  {
    path: '/others/banner',
    component: BannerPopup
  },
  {
    path: '/others/banner_detail/:id',
    props: true,
    component: BannerDetail
  }
].concat(
  V2Router.lifeplan,
  V1Router.investment,
  V1Router.product,
  V1Router.payment,
  V2Router.portfolio,
  V2Router.investment,
  V2Router.product,
  V2Router.payment,
  V1Router.lifeplan,
  V1Router.portfolio,
  V2Router.market,
  V2Router.allocation
)
