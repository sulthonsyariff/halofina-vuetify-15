import { checkFeatures } from '@/services/Helpers'

// Rename v1 to v0
function renameApi (route, dontExecuteRename) {
  let envJava = checkFeatures('halofina:trx').version > 2
  if (envJava === true) {
    return route
  } else {
    if (dontExecuteRename === true) {
      return route
    } else {
      return route.replace('v1', 'v0')
    }
  }
}

export default (url, method, dontExecuteRename, data) => {
  return fetch(process.env.API_SERVER + renameApi(url, dontExecuteRename), {
    method,
    headers: {
      'X-Authorization': `Bearer ${localStorage.getItem('halofina-token')}`,
      'X-Api-Key': process.env.API_KEY,
      'Content-Type': method === 'POST' ? 'application/json' : null
    },
    body: method === 'POST' ? JSON.stringify(data) : null
  }).then((response) => {
    if (response.status !== 200) {
      throw response
    }

    return response.json()
  }).then((response) => {
    if (response.success !== true) {
      throw response
    }
    
    return response
  })
}
