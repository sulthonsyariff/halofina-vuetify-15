import { track } from '@/services/Analytics'

export const filters = {}
export function bakeFilter(url, handlers) {
  const steps = url.split('/')
  let currentStep = filters
  steps.map(step => {
    if (!currentStep[step]) currentStep[step] = {}
    currentStep = currentStep[step]
  })
  currentStep['/'] = handlers
}

function addFilterGroup(groupName) {
  const filterGroup = require('./filters/' + groupName)
  for (const key in filterGroup) {
    bakeFilter(key, filterGroup[key])
  }
}

addFilterGroup('all')
addFilterGroup('dashboard')
addFilterGroup('finapedia')
addFilterGroup('finacheck')

export function getFilter(steps) {
  let currentStep = filters

  const varSteps = []
  let first = true
  for (const step of steps) {
    if (first || step.length) {
      first = false
      if (currentStep[step]) currentStep = currentStep[step]
      else if (currentStep['*']) {
        currentStep = currentStep['*']
        varSteps.push(step)
      } else {
        currentStep = undefined
        break
      }
    }
  }
  return {
    handlers: currentStep ? currentStep['/'] : undefined,
    varSteps,
  }
}
export function getClickHandler(handlers, el) {
  // check id
  if (handlers.clickedIds && el.id) {
    if (handlers.clickedIds[el.id])
      return handlers.clickedIds[el.id]
    if (handlers.clickedIds['*'])
      return handlers.clickedIds['*']
  }
  // check classes
  if (handlers.clickedClasses && el.classList) {
    for (const className of el.classList) {
      if (handlers.clickedClasses[className])
        return handlers.clickedClasses[className]
    }
    if (handlers.clickedClasses['*'])
      return handlers.clickedClasses['*']
  }
}

function actuallyTrack(event) {
  track(...event)
}
export function setActualTracker(actualFunction) {
  // eslint-disable-next-line no-func-assign
  actuallyTrack = actualFunction
}

let lastUrl
let lastEventType
let pageProps
let previousProps
let filter
let handlers

export function tracker(event, customProps, customUrl) {
  console.log('ehehehasas', event)
  const el = event.target || {}
  let url = customUrl || location.href
  // only use the part after ".com" or port and before params
  const parts = url.match(
    /(https?:\/\/)?([a-z|0-9|:|.]*)\/([#|a-z|0-9|/]*)(\?.*)?/
  )

  url = parts[3] || ''
  const steps = url.split('/')

  // if the url has changed, get a new handler
  let newPage = false
  if (lastUrl != url) {
    filter = getFilter(steps)
    lastUrl = url
    newPage = true
    handlers = filter.handlers
  }

  // if the handlers object exists, get the specific handler
  let handler
  const props = {}

  if (handlers) {
    switch (event.type) {
      case 'click':
        handler = getClickHandler(handlers, el)
        break
      case 'page opened':
        // prevent double page opening
        if (newPage || lastEventType != 'page opened') {
          handler = handlers.opened
          props.opened = Date.now()
        }
        break
      case 'page rendered':
        handler = handlers.rendered
        props.rendered = Date.now()
        break
      case 'page closed':
        handler = handlers.closed
        break
      case 'scroll':
        handler = handlers.scroll
        break
    }
  }

  lastEventType = event.type

  // if the handler exists, collect the props
  // and call the actual tracking function
  if (handler) {
    props.url = url
    props.steps = steps
    props.varSteps = filter.varSteps
    props.timestamp = Date.now()
    props.id = el.id
    props.classes = el.classList
    props.previousProps = previousProps

    previousProps = props
    if (newPage) pageProps = props

    props.pageProps = pageProps

    // apply custom props
    if (customProps) {
      for (const key in customProps) {
        props[key] = customProps[key]
      }
    }

    setTimeout(() => {
      props.resultClasses = el.classList
      actuallyTrack(handler(props))
      // console.log(props)
    }, 10)
  }
}

let delay = 500
export function setScrollDelay(newDelay) {
  delay = newDelay
}
let delayed
function debouncedOnScroll() {
  if (!delayed) {
    delayed = setTimeout(() => {
      delayed = null

      const scrollHeight =
        (document.documentElement.scrollHeight || 100) -
        window.innerHeight

      tracker(
        {
          type: 'scroll',
          target: { id: 'window' },
        },
        {
          scrollPosition: window.top.scrollY,
          scrollFraction: window.top.scrollY / scrollHeight,
        }
      )
    }, delay)
  }
}
export function startTrackingScroll() {
  window.removeEventListener('scroll', debouncedOnScroll)
  window.addEventListener('scroll', debouncedOnScroll)
}
export function stopTrackingScroll() {
  window.removeEventListener('scroll', debouncedOnScroll)
}

/**
 * Filter Interface:
 * [url/string] (can use * as wild card for each step): {
 *  opened / rendered / closed / scrolled (props) {
 *    do any logic and operations with the props here
 *    return: 3rd party tracker format
 *  }
 *  clickedIds / clickedClasses: {
 *    [id / class]: can use * as wild card
 *      idem
 *  }
 * }
 *
 * Filter Props:
 * all:
 * - url: string (after halofina.com/)
 * - steps: string[]
 * - varSteps: string[] (url step from wildcards)
 * - timestamp: number
 * - id: string (element's id)
 * - classes: {[string]: boolean} (element's classes)
 * - resultClasses: {[string]: boolean} (element's classes after 10 ms)
 * - previousEvent: FilterProps
 * - ...: custom props, automatically added to the result
 * opened:
 * - opened: number
 * rendered:
 * - rendered: number
 * closed:
 * -
 * scrolled:
 * - scrollPosition: number
 * - scrollFraction: number
 * - opened: number
 * - rendered: number
 *
 */
