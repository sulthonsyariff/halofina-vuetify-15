module.exports = {
  'finacheck/kyc/personal': {
    clickedIds: {
      'save-btn': props => ['KYC, Personal Saved'],
    },
  },
  'finacheck/kyc/address': {
    clickedIds: {
      'save-btn': props => ['KYC, Address Saved'],
    },
  },
  'finacheck/kyc/bank': {
    clickedIds: {
      'save-btn': props => ['KYC Bank Saved', {
        id: 10
      }],
    },
  },
  'finacheck/kyc/list': {
    clickedIds: {
      'submit-btn': props => ['KYC Submitted'],
    },
  }
}