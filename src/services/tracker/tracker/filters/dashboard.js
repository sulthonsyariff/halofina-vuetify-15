module.exports = {
  dashboard: {
    clickedClasses: {
      '*': props => [
        'Dashboard Button Clicked',
        {
          to: props.id,
        },
      ],
    },
  },
}
