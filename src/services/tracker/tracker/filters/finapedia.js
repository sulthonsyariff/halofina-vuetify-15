module.exports = {
  'finapedia/glossary': {
    clickedIds: {
      'glossary-list': props => [
        'Glossary Selected',
        {
          title: props.id,
        },
      ],
    },
  }
}
