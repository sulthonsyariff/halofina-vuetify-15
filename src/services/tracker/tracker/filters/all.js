module.exports = {
  '*': {
    opened: props => [
      'Page Opened',
      {
        url: props.url,
      },
    ],
    rendered: props => [
      'Page Rendered',
      {
        url: props.url,
        classes: props.classes,
        after_opened: props.timestamp - props.opened,
      },
    ],
    closed: props => [
      'Page Closed',
      {
        url: props.url,
        after_rendered: props.timestamp - props.rendered,
      },
    ],
    scroll: props => [
      'Page Scrolled',
      {
        url: props.url,
        scroll_position: props.scrollPosition,
        scroll_percentage: props.scrollPercentage,
        after_rendered: props.afterRendered,
      },
    ],
  },
}
