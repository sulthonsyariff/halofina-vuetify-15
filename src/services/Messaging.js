import { getOS } from './Helpers'

export function tokenAndroid () {
  var tokenAndroid = android.getFirebaseToken()
  localStorage.setItem('halofina-fcm-token', tokenAndroid)
  return tokenAndroid
}

export function tokenIos () {
  var tokenIos = ''
  window.bridge.post('firebasetoken', {}, (results, error) => {
    localStorage.setItem('halofina-fcm-token', results.token)
    tokenIos = results.token
  })
  return tokenIos
}

export function routingFromNotificationAndroid () {
  if (typeof android !== 'undefined') {
    var route = android.getRouteFromNotification()
    localStorage.setItem('halofina-fcm-route', route)
    return route
  }
}

export function routingFromNotificationIos () {
  var route = ''
  window.bridge.post('routing', {}, (results, error) => {
     localStorage.setItem('halofina-fcm-route', results.route)
     route = results.route
  })
  return route
}

export function controlCrispVisibility (condition) {
  if (condition === true) {
    $crisp.push(['do', 'chat:show'])
  } else {
    $crisp.push(['do', 'chat:hide'])
  }
}

export function setEmailForCrisp (email) {
  $crisp.push(["set", "user:email", email])
}

export function setNameForCrisp (name) {
  $crisp.push(["set", "user:nickname", name])
}

export function resetCrispSession () {
  $crisp.push(["do", "session:reset", [false]])
}

export function openCrisp () {
  $crisp.push(["do", "chat:open"])
}

export function closeCrisp () {
  $crisp.push(["do", "chat:close"])
}

export function booleanOpen() {
  $crisp.is("chat:opened")
}
