import { getOS } from './Helpers'

export function track (name, property) {
  return window.mixpanel.track(name, property)
}

export function userProperties (property) {
  return window.mixpanel.people.set(property)
}

export function setAlias (value) {
  return window.mixpanel.alias(value)
}

export function setIdentify (value) {
  return window.mixpanel.identify(value, {
    userId: value
  })
}

export function getConversionDataAndroid () {
  var promise = new Promise( function(resolve, reject) {
    var timesRun = 0
    var conversionData = ''
    var interval = setInterval(() => {
      if (typeof android !== 'undefined') {
        timesRun += 1
        conversionData = android.getAttributionData() || null
        if (conversionData !== null) {
          clearInterval(interval)
          resolve(conversionData)
        } else if (timesRun === 100) {
          clearInterval(interval)
          reject(new Error("Tidak dapat mengambil attribution data"))
        }
      }
    }, 500)
  })
  return promise
}

export function getConversionDataIos () {
  var promise = new Promise( function(resolve, reject) {
    var timesRun = 0
    var conversionData = ''
    var interval = setInterval(() => {
      timesRun += 1
      window.bridge.post('conversiondata', {}, (results, error) => {
        conversionData = results.conversiondataaf
      })
      if (conversionData !== null) {
        clearInterval(interval)
        resolve(conversionData)
      } else if (timesRun === 100) {
        clearInterval(interval)
        reject(new Error("Tidak dapat mengambil attribution data"))
      }
    }, 500)
  })
  return promise
}

export function setConversionDatatoMixpanel (conversionData) {
  if (conversionData.af_status === 'Organic') {
    track('App Load', {
      campaign: 'Organic',
      media_source: 'Organic'
    })
    return window.mixpanel.register_once({
      campaign: 'Organic',
      media_source: 'Organic'
    })      
  } else {
    track('App Load', {
      campaign: conversionData.campaign,
      media_source: conversionData.media_source
    })
    return window.mixpanel.register_once({
      campaign: conversionData.campaign,
      media_source: conversionData.media_source
    })
  }
}

export function trackRevenue (total) {
  var os = getOS()
  var options = {
    eventName: "track_revenue",
    eventValues: "{\"af_revenue\":\"" + Number(total) +"\"}"
  }
  if (process.env.SENTRY_ENV === '"production"') {
    if (os === 'android') {
      android.trackRevenue(options.eventName, options.eventValues)
    } else {
      window.bridge.post('track_revenue', {'eventName': 'track_revenue', 'eventValues': '{\"af_revenue\":\"' + Number(total) +'\"}'})
    }
  }
}