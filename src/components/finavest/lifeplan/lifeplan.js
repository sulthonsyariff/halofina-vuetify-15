// Lifeplan
const LifeplanRecommendation = () => import('@/components/finavest/lifeplan/LifeplanRecommendation')
const LifeplanEdit = () => import('@/components/finavest/lifeplan/LifeplanEdit')
const LifeplanTemplate = () => import('@/components/finavest/lifeplan/LifeplanTemplate')
const LifeplanAdd = () => import('@/components/finavest/lifeplan/LifeplanAdd')
const LifeplanChoose = () => import('@/components/finavest/lifeplan/LifeplanChoose')

export default [
  // lifeplan
  {
    path: '/finavest/lifeplan/add',
    component: LifeplanAdd
  },
  {
    path: '/finavest/lifeplan/add/:id',
    component: LifeplanAdd,
    props: true
  },
  {
    path: '/finavest/lifeplan/choose/:data',
    props: true,
    component: LifeplanChoose
  },
  {
    path: '/finavest/lifeplan/template',
    component: LifeplanTemplate
  },
  {
    path: '/finavest/lifeplan/recommendation',
    component: LifeplanRecommendation
  },
  {
    path: '/finavest/lifeplan/template/:id',
    props: true,
    component: LifeplanTemplate
  },
  {
    path: '/finavest/lifeplan/recommendation/:id',
    props: true,
    component: LifeplanRecommendation
  },
  {
    path: '/finavest/lifeplan/edit/:id/:dashboard',
    props: true,
    component: LifeplanEdit
  },
  {
    path: '/finavest/lifeplan/edit/:id/:dashboard/:burger',
    props: true,
    component: LifeplanEdit
  }
]
