// Portfolio
const PortfolioDashboard = () => import('@/components/finavest/portfolio/Dashboard')
const PortfolioDetail = () => import('@/components/finavest/portfolio/DetailPortfolio')

export default [
  // Portfolio
  {
    path: '/finavest/portfolio',
    component: PortfolioDashboard
  },
  {
    path: '/finavest/portfolio/detail/:id',
    component: PortfolioDetail,
    props: true
  }
]
