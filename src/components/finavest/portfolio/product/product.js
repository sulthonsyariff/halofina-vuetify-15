const ListProduct = () => import('@/components/finavest/product/ProductList')
// Portfolio Product
const ProductDetail = () => import('@/components/finavest/product/reksadana/ProductDetail')
const ProductDetailAset = () => import('@/components/finavest/product/reksadana/ProductDetailAset')
const ProductDetailAsetEmas = () => import('@/components/finavest/product/gold/ProductDetailAsetEmas')
const ProductDetailEmas = () => import('@/components/finavest/product/gold/ProductDetailEmas')

export default [
  {
    path: '/finavest/investment/buy/list_product/:type',
    props: true,
    component: ListProduct
  },
  // Portfolio Product
  {
    path: '/finavest/portfolio/product/detail/:id/:fromListProduct',
    props: true,
    component: ProductDetail
  },
  {
    path: '/finavest/portfolio/product/detail/:lifeplanid/:productid/:fromListProduct',
    props: true,
    component: ProductDetail
  },
  {
    path: '/finavest/portfolio/product/detailaset/:lifeplanid/:productid/:id',
    props: true,
    component: ProductDetailAset
  },
  {
    path: '/finavest/portfolio/product/detailasetemas/:lifeplanid/:productid/:id',
    props: true,
    component: ProductDetailAsetEmas
  },
  {
    path: '/finavest/portfolio/product/detail_emas/:id/:fromListProduct',
    props: true,
    component: ProductDetailEmas
  },
  {
    path: '/finavest/portfolio/product/detail_emas/:lifeplanid/:portfolioid/:fromListProduct',
    props: true,
    component: ProductDetailEmas
  }
]
