// Investment Buy Per Lifeplan
import BuyInvestment from '@/components/finavest/investment/buy/BuyInvestment'
import { checkFeatures } from '@/services/Helpers'
// Investment Buy
const BuyInvestmentDetail = () => import('@/components/finavest/investment/buy/BuyInvestmentDetail')
// Investment Sell
const SellList = () => import('@/components/finavest/investment/sell/SellList')
const SellConfirm = () => import('@/components/finavest/investment/sell/SellConfirm')
const SellProduct = () => import('@/components/finavest/investment/sell/SellProduct')
// Finapoint
const FinapointList = () => import('@/components/finavest/investment/finapoint/FinapointList')
// Voucher
const DetailVoucherCard = () => import('@/components/finavest/investment/voucher/DetailVoucherCard')
const canAccessNewVoucher = checkFeatures('halofina:detail_voucher').version
export default [
  // Investment Buy
  {
    path: '/finavest/investment/buy/buy_investment/:id/:type',
    props: true,
    component: BuyInvestment
  },
  {
    path: '/finavest/investment/buy/detail',
    component: BuyInvestmentDetail
  },
  // Investment Sell
  {
    path: '/finavest/investment/sell/sell_list/:id',
    props: true,
    component: SellList
  },
  {
    path: '/finavest/investment/sell/sell_product/:lifeplanId/:productId',
    props: true,
    component: SellProduct
  },
  {
    path: '/finavest/investment/sell/sell_confirm',
    component: SellConfirm
  },
  // Voucher
  {
    path: '/finavest/investment/detail_voucher_card',
    component: DetailVoucherCard,
    beforeEnter: (to, from, next) => {
      // can access allocation RD
      if (canAccessNewVoucher > 1) {
        next('/v2/finavest/investment/detail_voucher_card')
      } else {
        next()
      }
    }
  },
  // Finapoint
  {
    path: '/finavest/investment/finapoint_list',
    component: FinapointList
  }
]
