import { checkFeatures } from '@/services/Helpers'
const canAccessAllocation = checkFeatures('halofina:allocation').enabled
const ListProduct = () => import('@/components/finavest/product/ProductList')
// Portfolio Product
const ProductDetail = () => import('@/components/finavest/product/reksadana/ProductDetail')
const ProductDetailAset = () => import('@/components/finavest/product/reksadana/ProductDetailAset')
const ProductDetailAsetEmas = () => import('@/components/finavest/product/gold/ProductDetailAsetEmas')
const ProductDetailEmas = () => import('@/components/finavest/product/gold/ProductDetailEmas')

export default [
  {
    path: '/finavest/investment/buy/list_product/:type',
    props: true,
    component: ListProduct
  },
  // Portfolio Product
  {
    path: '/finavest/portfolio/product/detail/:id/:fromListProduct',
    props: true,
    component: ProductDetail
  },
  {
    path: '/finavest/portfolio/product/detailaset/:lifeplanid/:productid/:accountid',
    props: true,
    component: ProductDetailAset,
    beforeEnter: (to, from, next) => {
      // can access allocation RD
      if (canAccessAllocation) {
        next('/v2/finavest/portfolio/product/detailaset/' + to.params.lifeplanid + '/' + to.params.productid + '/' + to.params.accountid)
      } else {
        next()
      }
    }
  },
  {
    path: '/finavest/portfolio/product/detailasetemas/:lifeplanid/:productid/:accountid',
    props: true,
    component: ProductDetailAsetEmas,
    beforeEnter: (to, from, next) => {
      // can access allocation RD
      if (canAccessAllocation) {
        next('/v2/finavest/portfolio/product/detailasetemas/' + to.params.lifeplanid + '/' + to.params.productid + '/' + to.params.accountid)
      } else {
        next()
      }
    }
  },
  {
    path: '/finavest/portfolio/product/detail_emas/:id/:fromListProduct',
    props: true,
    component: ProductDetailEmas
  },
  {
    path: '/finavest/portfolio/product/detail_emas/:lifeplanid/:portfolioid/:fromListProduct',
    props: true,
    component: ProductDetailEmas
  }
]
