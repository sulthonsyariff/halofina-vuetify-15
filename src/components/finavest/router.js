// module router
import lifeplan from './lifeplan/lifeplan.js'
import portfolio from './portfolio/portfolio.js'
import investment from './investment/investment.js'
import product from './product/product.js'
import payment from './payment/payment.js'

export default {
  lifeplan: lifeplan,
  portfolio: portfolio,
  investment: investment,
  product: product,
  payment: payment
}
