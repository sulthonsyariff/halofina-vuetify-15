import { checkFeatures } from '@/services/Helpers'
const canAccessNewPaymentMethod = checkFeatures('halofina:payment_method').version
// Payment
const PaymentBank = () => import('@/components/finavest/payment/Bank')
const PaymentDetail = () => import('@/components/finavest/payment/Detail')
const PaymentHistory = () => import('@/components/finavest/payment/history/History')
const PaymentInvoice = () => import('@/components/finavest/payment/Invoice')

export default [
  // Payment
  {
    path: '/finavest/payment/bank',
    component: PaymentBank
  },
  {
    path: '/finavest/payment/history',
    component: PaymentHistory
  },
  {
    path: '/finavest/payment/detail/:id/:type/:access',
    props: true,
    component: PaymentInvoice
  },
  {
    path: '/finavest/payment/invoice/:id/:fromBankPage/:trxid/:type',
    props: true,
    component: PaymentDetail,
    beforeEnter: (to, from, next) => {
      // If payment method version more than 1
      if (canAccessNewPaymentMethod > 1) {
        next('/v2/finavest/payment/invoice/' + to.params.id + '/' + to.params.fromBankPage + '/' + to.params.trxid + '/' + to.params.type)
      } else {
        next()
      }
    }
  }
]
