// FCU
const PasswordChange = () => import('./PasswordChange')
const PasswordDone = () => import('./PasswordDone')
const PasswordOtp = () => import('./PasswordOtp')

export default [
  {
    path: '/password/forget/change',
    component: PasswordChange
  },
  {
    path: '/password/forget/done',
    component: PasswordDone
  },
  {
    path: '/password/forget/otp',
    component: PasswordOtp
  }
]
