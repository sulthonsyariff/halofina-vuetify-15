// FCU
const AllResult = () => import('@/components/v2/finacheck/fcu/AllResult')
const ArusResult = () => import('@/components/v2/finacheck/fcu/ArusResult')
const NeracaResult = () => import('@/components/v2/finacheck/fcu/NeracaResult')
const FormNeraca = () => import('@/components/v2/finacheck/fcu/FormNeraca')
const FormNeraca2 = () => import('@/components/v2/finacheck/fcu/FormNeraca2')
const FormArus = () => import('@/components/v2/finacheck/fcu/FormArus')
const FcuOnboarding = () => import('@/components/v2/finacheck/fcu/Onboarding')
const FcuListData = () => import('@/components/v2/finacheck/fcu/ListData')
const FcuArus = () => import('@/components/v2/finacheck/fcu/Arus')

export default [
  // FCU
  {
    path: '/fcu',
    component: FcuOnboarding
  },
  {
    path: '/fcu/list',
    component: FcuListData
  },
  {
    path: '/fcu/arus',
    component: FcuArus
  },
  {
    path: '/fcu/arus/form',
    component: FormArus
  },
  {
    path: '/fcu/neraca/form',
    component: FormNeraca
  },
  {
    path: '/fcu/neraca/form2',
    component: FormNeraca2
  },
  {
    path: '/fcu/neraca/result',
    component: NeracaResult
  },
  {
    path: '/fcu/arus/result',
    component: ArusResult
  },
  {
    path: '/fcu/allresult',
    component: AllResult
  }
]
