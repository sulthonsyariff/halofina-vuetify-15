import LoginEmail from '@/components/v2/finacheck/login/LoginEmail'
import LoginPassword from '@/components/v2/finacheck/login/LoginPassword'
import TheSignUp from '@/components/v2/finacheck/signup/TheSignUp'
import TheChoosePage from '@/components/v2/finacheck/TheChoosePage'
import pin from './pin/pin.js'
import kyc from './kyc/kyc.js'
import rpa from './rpa/rpa.js'
import fqt from './fqt/fqt.js'
import fcu from './fcu/fcu.js'
import password from './password/forget/forget.js'
const LoginEmailOld = () => import('@/components/v2/finacheck/login/LoginEmailOld')

export default {
  password: password,
  pin: pin,
  rpa: rpa,
  kyc: kyc,
  fqt: fqt,
  fcu: fcu,
  acquisitions: [
    {
      path: '/login/email',
      component: LoginEmail
    },
    {
      path: '/blah_blah_blah_stop_here_12345',
      component: LoginEmailOld
    },
    {
      path: '/login/password',
      component: LoginPassword
    },
    {
      path: '/signup/v2',
      component: TheSignUp
    },
    {
      path: '/choose',
      component: TheChoosePage
    }
  ]
}
