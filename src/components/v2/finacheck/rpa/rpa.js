// Risk Profile
const RpaNewInput = () => import('@/components/v2/finacheck/rpa/Input')
// const RiskProfile = () => import('@/components/v2/finacheck/rpa/RiskProfile')
// const RiskProfile2 = () => import('@/components/v2/finacheck/rpa/RiskProfile2')
// const RiskProfile3 = () => import('@/components/v2/finacheck/rpa/RiskProfile3')
// const RiskProfile4 = () => import('@/components/v2/finacheck/rpa/RiskProfile4')
// const RiskProfile5 = () => import('@/components/v2/finacheck/rpa/RiskProfile5')
// const RiskProfile6 = () => import('@/components/v2/finacheck/rpa/RiskProfile6')
// const Result = () => import('@/components/v2/finacheck/rpa/Result')

export default [
  // RPA
  {
    path: '/new/finacheck/rpa/input/:edit',
    props: true,
    component: RpaNewInput
  }
  // {
  //   path: '/rpa/result',
  //   component: Result
  // },
  // {
  //   path: '/new/riskprofile',
  //   component: RiskProfile
  // },
  // {
  //   path: '/new/riskprofile2',
  //   component: RiskProfile2
  // },
  // {
  //   path: '/new/riskprofile3',
  //   component: RiskProfile3
  // },
  // {
  //   path: '/new/riskprofile4',
  //   component: RiskProfile4
  // },
  // {
  //   path: '/new/riskprofile5',
  //   component: RiskProfile5
  // },
  // {
  //   path: '/new/riskprofile6',
  //   component: RiskProfile6
  // }
]
