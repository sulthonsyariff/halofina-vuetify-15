export default [
  {
    path: '/pin/edit',
    component: () => import('./edit/PinEdit')
  },
  {
    path: '/pin/forget',
    component: () => import('./forget/PinForget')
  },
  {
    path: '/pin/input',
    component: () => import('./verify/PinInput')
  },
  {
    path: '/pin/verification',
    component: () => import('./verify/PinVerification')
  },
  {
    path: '/pin/new',
    component: () => import('./new/PinNew')
  },
  {
    path: '/pin/new/confirm',
    component: () => import('./new/PinConfirm')
  }
]
