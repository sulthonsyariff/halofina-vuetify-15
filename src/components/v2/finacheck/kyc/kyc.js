// KYC
const InputKycList = () => import('@/components/v2/finacheck/kyc/input/ListKyc')
const InputKycPersonal = () => import('@/components/v2/finacheck/kyc/input/Personal')
const InputKycAdditionalPersonal = () => import('@/components/v2/finacheck/kyc/input/AdditionalPersonal')
const InputKycJob = () => import('@/components/v2/finacheck/kyc/input/Job')
const InputKycAddress = () => import('@/components/v2/finacheck/kyc/input/Address')
const InputKycBank = () => import('@/components/v2/finacheck/kyc/input/Bank')
const InputKycEmail = () => import('@/components/v2/finacheck/kyc/input/Email')
const NewKycSK = () => import('@/components/v2/finacheck/kyc/input/SK')
const NewKycVerification = () => import('@/components/v2/finacheck/kyc/input/Verification')
const NewKycList = () => import('@/components/v2/finacheck/kyc/ListKyc')
const NewKycProcess = () => import('@/components/v2/finacheck/kyc/Process')

export default [
  // KYC
  {
    path: '/v2/input/finacheck/kyc/list/:flag',
    component: InputKycList
  },
  {
    path: '/v2/input/finacheck/kyc/personal/:flag/:submit',
    component: InputKycPersonal
  },
  {
    path: '/v2/input/finacheck/kyc/additional_personal/:flag/:submit',
    component: InputKycAdditionalPersonal
  },
  {
    path: '/v2/input/finacheck/kyc/address/:flag/:submit',
    component: InputKycAddress
  },
  {
    path: '/v2/input/finacheck/kyc/bank/:flag/:submit',
    component: InputKycBank
  },
  {
    path: '/v2/input/finacheck/kyc/job/:flag/:submit',
    component: InputKycJob
  },
  {
    path: '/v2/input/finacheck/kyc/email',
    component: InputKycEmail
  },
  {
    path: '/v2/input/finacheck/kyc/sk',
    component: NewKycSK
  },
  {
    path: '/v2/input/finacheck/kyc/verification',
    component: NewKycVerification
  },
  // view in profile
  {
    path: '/v2/finacheck/kyc/list',
    component: NewKycList
  },
  {
    path: '/v2/finacheck/kyc/process',
    component: NewKycProcess
  }
]
