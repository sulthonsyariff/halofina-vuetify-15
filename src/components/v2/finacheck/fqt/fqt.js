const FQTInput = () => import('@/components/v2/finacheck/fqt/FQTInput')
const FQTInput2 = () => import('@/components/v2/finacheck/fqt/FQTInput2')
const FQTResults = () => import('@/components/v2/finacheck/fqt/FQTResults')
const FQTResults2 = () => import('@/components/v2/finacheck/fqt/FQTResults2')
const FQTIntro = () => import('@/components/v2/finacheck/fqt/FQTIntro')

export default [
  // FQT
  {
    path: '/v2/fqt/results',
    component: FQTResults
  },
  {
    path: '/v2/fqt/results/2',
    component: FQTResults2
  },
  {
    path: '/v2/fqt/intro',
    component: FQTIntro
  },
  {
    path: '/v2/fqt/input',
    component: FQTInput
  },
  {
    path: '/v2/fqt/input/2',
    component: FQTInput2
  }
]
