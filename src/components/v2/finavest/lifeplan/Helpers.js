import Api from '@/services/Api'

export async function getRecommendation (duration, inflation, type, assets, detail, currentCost) {
  let data = null
  await Api('finavest/v1/recommendation', {
    id_risk_profile: Number(localStorage.getItem('halofina-rpa')),
    current_age: detail ? detail.current_age : 0,
    retirement_age: detail ? detail.retirement_age : 0,
    duration: duration,
    inflation: Number(inflation),
    current_cost: type === 'RETIREMENT' ? (currentCost * 0.7) * duration : currentCost,
    type: type,
    asset: assets
  }, true)
    .then((res) => {
      data = res
    })
    .catch((err) => {
      return err
    })
  return data
}
