// Dashboard
const LifeplanDashboard = () => import('@/components/v2/finavest/lifeplan/Dashboard')
// Add Choose
const LifeplanAdd2 = () => import('@/components/v2/finavest/lifeplan/Add')
// Form
const LifeplanEdit2 = () => import('@/components/v2/finavest/lifeplan/Edit')
// Result Calculate
const CalculateResult = () => import('@/components/v2/finavest/lifeplan/CalculateResult')
// Set Priority
const SetPriority = () => import('@/components/v2/finavest/lifeplan/SetPriority')
// Adjust Lifeplan
const AdjustLifeplan = () => import('@/components/v2/finavest/lifeplan/AdjustLifeplan')
// Detail Lifeplan
const DetailLifeplan = () => import('@/components/v2/finavest/lifeplan/DetailLifeplan')
// Settings Lifeplan
const SettingsLifeplan = () => import('@/components/v2/finavest/lifeplan/Settings')

export default [
  // Lifeplan
  {
    path: '/finavest/new/lifeplan/dashboard',
    component: LifeplanDashboard
  },
  {
    path: '/finavest/new/lifeplan/add',
    component: LifeplanAdd2
  },
  {
    path: '/finavest/new/lifeplan/edit/:id/:dashboard',
    props: true,
    component: LifeplanEdit2
  },
  {
    path: '/finavest/new/lifeplan/calculate_result',
    props: true,
    component: CalculateResult
  },
  {
    path: '/finavest/new/lifeplan/setpriority',
    component: SetPriority
  },
  {
    path: '/finavest/new/lifeplan/adjust_lifeplan/:lifeplanId',
    props: true,
    component: AdjustLifeplan
  },
  {
    path: '/finavest/new/lifeplan/detail_lifeplan/:id',
    props: true,
    component: DetailLifeplan
  },
  {
    path: '/finavest/new/lifeplan/settings/:bool',
    props: true,
    component: SettingsLifeplan
  }
]
