// Zakat
const ZakatList = () => import('@/components/v2/finavest/payment/zakat/ZakatList')
const ZakatContainer = () => import('@/components/v2/finavest/payment/zakat/ZakatContainer')
const ZakatTypeDesc = () => import('@/components/v2/finavest/payment/zakat/ZakatTypeDesc')
const ZakatPayment = () => import('@/components/v2/finavest/payment/zakat/ZakatPayment')
const ZakatInvoice = () => import('@/components/v2/finavest/payment/zakat/ZakatInvoice')

export default [
  // Zakat
  {
    path: '/finavest/payment/zakat/zakat_list',
    component: ZakatList
  },
  {
    path: '/finavest/payment/zakat/zakat_container/:type',
    props: true,
    component: ZakatContainer
  },
  {
    path: '/finavest/payment/zakat/zakat_desc/:type',
    props: true,
    component: ZakatTypeDesc
  },
  {
    path: '/finavest/payment/zakat/zakat_payment/',
    component: ZakatPayment
  },
  {
    path: '/finavest/payment_zakat/detail/zakat/invoice/:idtrx/:access',
    props: true,
    component: ZakatInvoice
  }
]
