import zakat from './zakat/zakat.js'
import faspay from './faspay/faspay.js'
// Portfolio Ideal
const ValueResult = () => import('@/components/finavest/payment/ValueResult')
// Payment
const V2PaymentInvoice = () => import('@/components/v2/finavest/payment/Invoice')
const V2PaymentDetail = () => import('@/components/v2/finavest/payment/Detail')

export default [
  // Invoice
  {
    path: '/finavest/new/invoice/value_result',
    component: ValueResult
  },
  // Payment
  {
    path: '/v2/finavest/payment/detail/:id/:type/:access',
    props: true,
    component: V2PaymentInvoice
  },
  {
    path: '/v2/finavest/payment/invoice/:id/:fromBankPage/:trxid/:type',
    props: true,
    component: V2PaymentDetail
  }
].concat(faspay, zakat)
