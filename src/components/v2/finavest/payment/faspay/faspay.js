const FaspayMethods = () => import('./FaspayMethods')
const FaspayInvoice = () => import('./FaspayInvoice')

export default [
  {
    path: '/v2/finavest/payment/faspay/faspay_methods',
    component: FaspayMethods
  },
  {
    path: '/v2/finavest/payment/faspay/faspay_invoice/:trxid',
    props: true,
    component: FaspayInvoice
  }
]
