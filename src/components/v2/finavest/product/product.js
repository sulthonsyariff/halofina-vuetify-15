const V2ProductDetail = () => import('@/components/v2/finavest/product/reksadana/ProductDetail')
const V2ProductDetailAset = () => import('@/components/v2/finavest/product/reksadana/ProductDetailAset')
const V2ProductDetailAsetEmas = () => import('@/components/v2/finavest/product/gold/ProductDetailAsetEmas')
const V2ProductDetailEmas = () => import('@/components/v2/finavest/product/gold/ProductDetailEmas')
// Product List
const ProductList = () => import('@/components/v2/finavest/product/ProductList')

export default [
  // Product
  {
    path: '/finavest/new/product/product_list/:type',
    props: true,
    component: ProductList
  },
  {
    path: '/v2/finavest/portfolio/product/detail/:id/:fromListProduct',
    props: true,
    component: V2ProductDetail
  },
  {
    path: '/v2/finavest/portfolio/product/detail/:lifeplanid/:productid/:fromListProduct',
    props: true,
    component: V2ProductDetail
  },
  {
    path: '/v2/finavest/portfolio/product/detailaset/:lifeplanid/:productid/:accountid',
    props: true,
    component: V2ProductDetailAset
  },
  {
    path: '/v2/finavest/portfolio/product/detailasetemas/:lifeplanid/:productid/:accountid',
    props: true,
    component: V2ProductDetailAsetEmas
  },
  {
    path: '/v2/finavest/portfolio/product/detail_emas/:id/:fromListProduct',
    props: true,
    component: V2ProductDetailEmas
  },
  {
    path: '/v2/finavest/portfolio/product/detail_emas/:lifeplanid/:portfolioid/:fromListProduct',
    props: true,
    component: V2ProductDetailEmas
  }
]
