import V2BuyInvestment from '@/components/v2/finavest/investment/buy/BuyInvestment'
// Investment Buy
const V2BuyInvestmentDetail = () => import('@/components/v2/finavest/investment/buy/BuyInvestmentDetail')
// Investment Sell
const V2SellList = () => import('@/components/v2/finavest/investment/sell/SellList')
const V2SellConfirm = () => import('@/components/v2/finavest/investment/sell/SellConfirm')
const V2SellProduct = () => import('@/components/v2/finavest/investment/sell/SellProduct')
// Smart Saving
const SmartSavingDetail = () => import('@/components/v2/finavest/investment/smart_saving/SmartSavingDetails')
// Voucher Card
const ListVoucher = () => import('@/components/v2/finavest/investment/voucher/ListVoucher')
const DetailVoucherCard = () => import('@/components/v2/finavest/investment/voucher/DetailVoucherCard')
// Voucher Input
const DetailVoucher = () => import('@/components/v2/finavest/investment/voucher/DetailVoucher')

export default [
  // Voucher Card
  {
    path: '/finavest/investment/list_voucher',
    component: ListVoucher
  },
  {
    path: '/v2/finavest/investment/detail_voucher_card',
    component: DetailVoucherCard
  },
  // Voucher Input
  {
    path: '/finavest/investment/detail_voucher/:cashback',
    props: true,
    component: DetailVoucher
  },
  // Smart Saving
  {
    path: '/finavest/investment/smart_saving/detail/:id',
    props: true,
    component: SmartSavingDetail
  },
  // Investment Buy
  {
    path: '/v2/finavest/investment/buy/buy_investment/:id/:type',
    props: true,
    component: V2BuyInvestment
  },
  {
    path: '/v2/finavest/investment/buy/detail',
    component: V2BuyInvestmentDetail
  },

  // Investment Sell
  {
    path: '/v2/finavest/investment/sell/sell_list/:id',
    props: true,
    component: V2SellList
  },
  {
    path: '/v2/finavest/investment/sell/sell_product/:lifeplanId/:productId',
    props: true,
    component: V2SellProduct
  },
  {
    path: '/v2/finavest/investment/sell/sell_confirm',
    component: V2SellConfirm
  }
]
