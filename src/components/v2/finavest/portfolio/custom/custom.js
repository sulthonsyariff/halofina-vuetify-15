// Portfolio
const CustomPortfolioBrief = () => import('@/components/v2/finavest/portfolio/custom/PortfolioBrief')

export default [
  // Portfolio Product
  {
    path: '/finavest/new/portfolio/custom',
    component: CustomPortfolioBrief
  }
]
