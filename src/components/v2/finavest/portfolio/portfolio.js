import custom from './custom/custom'
// Portfolio
const PortfolioDetailV2 = () => import('@/components/v2/finavest/portfolio/Detail')
const PortfolioIdeal = () => import('@/components/v2/finavest/portfolio/PortfolioIdeal')
const PortfolioLifeplanDetail = () => import('@/components/v2/finavest/portfolio/LifeplanDetail')

export default [
  // Portfolio Product
  {
    path: '/finavest/new/portfolio/detail',
    component: PortfolioDetailV2
  },
  {
    path: '/finavest/new/portfolio/lifeplan/detail/:id',
    component: PortfolioLifeplanDetail
  },
  {
    path: '/finavest/new/portfolio/ideal/:flags',
    component: PortfolioIdeal
  }
].concat(custom)
