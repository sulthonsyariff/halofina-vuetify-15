// import { replaceDot } from '@/services/Helpers'

export function addToCart (product) {
  let array = []
  let carts = JSON.parse(localStorage.getItem('halofina-market-cart'))
  if (carts) {
    let isProductExist = checkProduct(product.product_name).isExist
    if (isProductExist === true) {
      console.log(editProduct(carts, product.product_id, product.amount))
      localStorage.setItem('halofina-market-cart', JSON.stringify(editProduct(carts, product.product_id, product.amount)))
    } else {
      carts.push(product)
      localStorage.setItem('halofina-market-cart', JSON.stringify(carts))
    }
  } else {
    array.push(product)
    localStorage.setItem('halofina-market-cart', JSON.stringify(array))
  }
}

export function checkProduct (productName) {
  let cart = JSON.parse(localStorage.getItem('halofina-market-cart'))
  let data = {
    isExist: false,
    amount: 0
  }
  if (cart) {
    for (let i = 0; i < cart.length; i++) {
      if (productName === cart[i].product_name) {
        data.isExist = true
        data.amount = cart[i].amount
      }
    }
  }
  return data
}

function editProduct (list, productId, amount) {
  for (let index = 0; index < list.length; index++) {
    if (list[index].product_id === productId) {
      list[index].amount = amount
    }
  }
  return list
}
