const MarketCard = () => import('./Cart')
const MarketSearch = () => import('./Search')
const MarketCategory = () => import('./Category')
const MarketProduct = () => import('./Product')
const MarketDashboard = () => import('./Dashboard')

export default [
  {
    path: '/finavest/market/dashboard',
    component: MarketDashboard
  },
  {
    path: '/finavest/market/cart',
    component: MarketCard
  },
  {
    path: '/finavest/market/search',
    component: MarketSearch
  },
  {
    path: '/finavest/market/category',
    component: MarketCategory
  },
  {
    path: '/finavest/market/product/:id',
    component: MarketProduct
  }
]
