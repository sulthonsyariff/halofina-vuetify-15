// allocation
const Target = () => import('@/components/v2/finavest/allocation/Target')
const Confirmation = () => import('@/components/v2/finavest/allocation/Confirmation')
const Completed = () => import('@/components/v2/finavest/allocation/Completed')

export default [
  {
    path: '/finavest/allocation/target/:lifeplanId/:assetId',
    props: true,
    component: Target
  },
  {
    path: '/finavest/allocation/confirmation',
    component: Confirmation
  },
  {
    path: '/finavest/allocation/completed',
    component: Completed
  }
]
