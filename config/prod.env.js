'use strict'

module.exports = {
  NODE_ENV: '"production"',
  API_SERVER: JSON.stringify(process.env.HALOFINA_API),
  API_VERSION: '"v1"',
  API_KEY: '"5c36fd38e143dde079d24f7807364ab6"',
  MOBILE_VERSION: '"v2"',
  SENTRY_DSN: '"https://b308f3e3f2534397863a4729886887ea@sentry.io/1334046"',
  SENTRY_ENV: '"' + process.env.SENTRY_ENV + '"',
  MIXPANEL_TOKEN: '"' + process.env.MIXPANELS_TOKEN + '"',
  MIXPANEL_DEBUG: false
}
