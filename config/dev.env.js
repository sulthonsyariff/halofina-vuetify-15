'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_SERVER: JSON.stringify(process.env.HALOFINA_API),
  SENTRY_DSN: '"https://b308f3e3f2534397863a4729886887ea@sentry.io/1334046"',
  SENTRY_ENV: '"' + process.env.SENTRY_ENV + '"',
  MIXPANEL_DEBUG: true,
  MIXPANEL_TOKEN: '"' + process.env.MIXPANELS_TOKEN + '"',
})
