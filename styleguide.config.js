const path = require('path')
const { VueLoaderPlugin } = require('vue-loader')

function resolve(dir) {
  return path.join(__dirname, '.', dir)
}

module.exports = {
  webpackConfig: {
    resolve: {
      alias: {
        "@": resolve("src")
      }
    },
    module: {
      rules: [
        {
          test: /\.vue$/,
          loader: "vue-loader"
        },
        {
          test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
          loader: "url-loader",
          options: {
            limit: 8192
          }
        },
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          loader: "babel-loader"
        },
        {
          test: /\.(css|scss)$/,
          use: ["vue-style-loader", "css-loader", "sass-loader"]
        },
        {
          test: /\.sass$/,
          use: [
            "vue-style-loader",
            "css-loader",
            {
              loader: "sass-loader",
              options: {
                sassOptions: {
                  indentedSyntax: true
                }
              }
            }
          ]
        }
      ]
    }
    // plugins: [new VueLoaderPlugin()]
  },
  renderRootJsx: "./styleguide.root.js",
  usageMode: "expand",
  styleguideDir: "public",
  ignore: ["**/__tests__/**", "**/README.md"],
  sections: [
    {
      name: "Introduction",
      content: "./README.md"
    },
    {
      name: "Components",
      components: "./src/components/reusable/test/**/[A-Z]*.vue"
    }
  ]
};
