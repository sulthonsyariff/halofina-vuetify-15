import Vue from 'vue'
import Vuetify from 'vuetify'
import LoginEmail from '@/components/v2/finacheck/login/LoginEmail'

Vue.use(Vuetify)

const Constructor = Vue.extend(LoginEmail)
const vm = new Constructor().$mount()

describe('LoginEmail.vue', () => {
  it('should set form email correctly', () => {
    // set data
    vm.email = 'halofinatest3@gmail.com'
    // test
    Vue.nextTick().then(() => {
      expect(vm.email).toBe('halofinatest3@gmail.com')
    })
  })

  // it('should post email with correct body and response', () => {
  //   vm.submit('halofinatest3@gmail.com')
  //     .then((res) => {

  //     })
  // })
})
