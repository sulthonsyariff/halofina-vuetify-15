import Vue from 'vue'
import Vuetify from 'vuetify'
import LoginPassword from '@/components/v2/finacheck/login/LoginPassword'

Vue.use(Vuetify)

const Constructor = Vue.extend(LoginPassword)
const vm = new Constructor().$mount()

describe('LoginPassword.vue', () => {
  it('should set form password correctly', () => {
    // set data
    vm.password = 'Harumanis2'
    // test
    Vue.nextTick().then(() => {
      expect(vm.password).toBe('Harumanis2')
    })
  })

  it('set user name correctly', () => {
    Vue.nextTick().then(() => {
      vm.getPersonalData()
        .then((res) => {
          // test
          expect(vm.name).toBe(res.d.fullname)
        })
    })
  })
})
